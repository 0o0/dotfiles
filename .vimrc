call pathogen#infect()
set nocompatible
syntax on
filetype plugin indent on
set shiftwidth=2
set tabstop=2
set expandtab

" Colorsche,e
colorscheme vividchalk

" CtrlP settings
set runtimepath^=~/.vim/bundle/ctrlp.vim
let g:ctrlp_match_window = 'bottom,order:ttb'
let g:ctrlp_switch_buffer = 0
let g:ctrlp_working_path_mode = 0

" Remappings
imap jk <Esc>

