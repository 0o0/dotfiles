# Author:
#   Thomas Graves <thomas@ooo.pm>
#   @dqt
#

# Source Prezto.
if [[ -s "${ZDOTDIR:-$HOME}/.zprezto/init.zsh" ]]; then
  source "${ZDOTDIR:-$HOME}/.zprezto/init.zsh"
fi

# Customize to your needs...
# tmux
alias tma='tmux attach -d -t'
alias tmn='tmux new -s $(basename $(pwd))'
alias tml='tmux list-sessions'

# Env {{{1
export RED='\033[0;31m'
export YELLOW='\033[1;33m'
export GREEN='\033[0;32m'
export RESET_COLOR='\033[0m'

export BROWSER=/usr/bin/chromium
export EDITOR=vim
export HISTSIZE=10000
# less colors {{{2
export LESS_TERMCAP_mb=$'\e[01;31m' # begin blinking
export LESS_TERMCAP_md=$'\e[01;34m' # begin bold
export LESS_TERMCAP_me=$'\e[0m'     # end mode
export LESS_TERMCAP_se=$'\e[0m'     # end standout-mode
export LESS_TERMCAP_so=$'\e[01;32m' # begin standout-mode - info box
export LESS_TERMCAP_ue=$'\e[0m'     # end underline
export LESS_TERMCAP_us=$'\e[01;36m' # begin underline

# Aliases {{{1
alias c="clear"
alias cleanvim="vim -N -u NONE"
alias cp="rsync --archive --human-readable --progress --verbose --whole-file"
alias scp="rsync --archive --checksum --compress --human-readable --itemize-changes --rsh=ssh --stats --verbose"
alias duh="du -h -d 0 [^.]*"
alias em="mutt"
alias htop="sudo htop"
alias inotify="echo fs.inotify.max_user_watches=524288 | sudo tee -a /etc/sysctl.conf && sudo sysctl -p"
alias install="sudo apt-get install"
alias l="ls -al"
alias ls='ls --color=auto'
alias monitor="sudo monitor"
alias myip="curl http://myip.dnsomatic.com && echo ''"
alias ocean="ssh -X vnc@107.170.86.141"
alias pretty-json="python2 -mjson.tool"
alias py="python"
alias syms="find . -maxdepth 1 -type l -print | while read line; do ls -alc "\$line"; done"
alias tran="transmission-remote-cli"
alias uninstall="sudo apt-get remove"
alias v="vim"

# Functions {{{1
cat <<EOF | nc "$1" "$2"
HEAD / HTTP/1.1
Host: host
Connection: close

EOF
recent() {
   find $HOME/Dropbox/ -type f -regex ".*\.\(md\|txt\)" -mtime -$1 -not -path "*dropbox*" -exec vim "{}" +
}
speedup() {
   </dev/null ffmpeg -i "$*" -filter atempo=1.5 "${*%%.mp3}-150.mp3"
}
textfiles() {
   file ${*:-*} | grep -E "text$" | sed 's/:.*//'
}
webrick() {
   port="${1:-3000}"
   ruby -r webrick -e "s = WEBrick::HTTPServer.new(:Port => $port, :DocumentRoot => Dir.pwd); trap('INT') { s.shutdown }; s.start"
}
# Path {{{1
pathadd() {
if [ -d "$1" ] && [[ ":$PATH:" != *":$1:"* ]]; then
    PATH="${PATH:+"$PATH:"}$1"
fi
}
